package cs380.othello;

import java.util.List;

public class OthelloPlayerBrett extends OthelloPlayer {
	
	private int depth    = 0;
	private int maxDepth = 0;
	
	public OthelloPlayerBrett(int maxDepth) {
		this.maxDepth = maxDepth;
	}
	
	@Override
	public OthelloMove getMove(OthelloState state) {
		depth = 0;
		System.out.println("Thinking of possible moves...");
		return minimaxDecision(state);
	}

	private OthelloMove minimaxDecision(OthelloState state) {
		OthelloMove bestMove = null;
		int bestValue		 = Integer.MIN_VALUE;
		
		List<OthelloMove> actions = state.generateMoves(1);
		
		depth++;
		for (int i = 0; i < actions.size(); i++) {
			OthelloState clone = state.applyMoveCloning(actions.get(i));
			
			int s = minimax(clone, maxDepth, true);
			//int s = minValue(state);
			
			if (s > bestValue) {
				bestValue = s;
				bestMove = actions.get(i);
			}
		}
		
		return bestMove;
	}

	/*
	 * Non-recursive implementation
	 * 
	private int minValue(OthelloState state) {
		int utilityValue = Integer.MAX_VALUE;
		
		if (state.gameOver() || depth >= maxDepth) {
			return Math.abs(state.score());
		}
		
		List<OthelloMove> actions = state.generateMoves(0);
		depth++;
		
		if (actions.isEmpty()) {
			utilityValue = state.score();
		}

		for (int i = 0; i < actions.size(); i++) {
			OthelloState clone =state.applyMoveCloning(actions.get(i));

			int s = maxValue(clone);
			
			if (s < utilityValue) {
				utilityValue = s;
			}
		}
		
		//System.out.println("Min UtilityValue: " + utilityValue);
		return utilityValue;
	}

	private int maxValue(OthelloState state) {
		int utilityValue = Integer.MIN_VALUE;
		
		if (state.gameOver() || depth >= maxDepth) {
			return Math.abs(state.score());
		}
		
		List<OthelloMove> actions = state.generateMoves(1);
		depth++;
		
		if (actions.isEmpty()) {
			utilityValue = state.score();
		}

		for (int i = 0; i < actions.size(); i++) {
			OthelloState clone = state.applyMoveCloning(actions.get(i));

			int s = minValue(clone);
			
			if (s > utilityValue) {
				utilityValue = s;
			}
		}
		
		//System.out.println("Max UtilityValue: " + utilityValue);
		return utilityValue;
	}
	*/
	
	public int minimax(OthelloState state, int depth, boolean turn) {
		
		//System.out.println(depth);
		
		if (state.gameOver() || depth == 0) {
	      return Math.abs(state.evaluation());
	    }

		if (turn) {
			int utilityValue = Integer.MIN_VALUE;
			List<OthelloMove> actions = state.generateMoves(1);
	      
			if (actions.isEmpty()) {
				return Math.abs(state.evaluation());
			} else {
				for (int i = 0; i < actions.size(); i++) {
					OthelloState clone = state.applyMoveCloning(actions.get(i));

					int s = minimax(clone, depth - 1, !turn);
	    
					if (s > utilityValue) {
						utilityValue = s;
					}
				}
			}
			return utilityValue;
	    } else {
	    	int utilityValue = Integer.MAX_VALUE;
	      
	    	List<OthelloMove> actions = state.generateMoves(0);

	    	if (actions.isEmpty()) {
	    		return state.evaluation();
	    	} else {
	    		for (int i = 0; i < actions.size(); i++) {
	        	
	    			OthelloState clone = state.applyMoveCloning(actions.get(i));

	    			int s = minimax(clone, depth - 1, !turn);
	          
	    			if (s < utilityValue) {
	    				utilityValue = s;
	    			}
	    		}
	    	}

	    	return utilityValue;
	    }
	  }
}
