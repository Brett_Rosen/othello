package cs380.othello;

import java.util.List;
import java.util.Random;

public class OthelloPlayerMCTS extends OthelloPlayer {

	public int iterations;
	
	public OthelloPlayerMCTS(int iterations) {
		this.iterations = iterations;
	}
	
	@Override
	public OthelloMove getMove(OthelloState state) {
		return monteCarloTreeSearch(state, iterations);
	}

	public OthelloMove monteCarloTreeSearch(OthelloState root, int iterations) {
		
		for (int i = 0; i < iterations; i++) {
			OthelloState state = treePolicy(root);
			
			if (state != null) {
				OthelloState state2 = defaultPolicy(state);
				int state2Score = state2.score();
			    backup(state,state2Score);
			}
		}
		
		try {
			return bestChild(root).associatedAction;
		} catch(NullPointerException e) {	
			
		}
		System.out.println("bad");
		return null;
	}

	private void backup(OthelloState state, int score) {
		state.visited += 1;
		state.avgScore = score;
		
		if(state.parent != null) {
			backup(state.parent, score);
		}
	}

	private OthelloState defaultPolicy(OthelloState state) {
		// Uses random agents to select actions until game is over, return final state
		OthelloPlayer players[] = {new OthelloRandomPlayer(),
				   				   new OthelloRandomPlayer()};
		do{
			OthelloMove move = players[state.nextPlayerToMove].getMove(state);            
			state = state.applyMoveCloning(move);            
		} while(!state.gameOver());
		
		return state;
	}

	private OthelloState treePolicy(OthelloState state) {
		boolean included = false;

		List<OthelloMove> moves = state.generateMoves();
		
		// Node is a terminal node, no moves to make
		if (moves.size() == 0) {
			return state;
		}
		
		// If node has child that is not in tree, add it as a child and return the node
		for(int i = 0; i < moves.size(); i++) {
			OthelloState newState = state.applyMoveCloning(moves.get(i));
			
			for (int j = 0; j < state.children.size(); j++) {
				if (newState == state.children.get(j)) {
					included = true;
				}
			}
			
			if (!included) {
				state.children.add(newState);
				newState.parent = state;
				newState.associatedAction = moves.get(i);
				return newState;
			}
			included = false;
		}
		
		// If all children are in tree, recall treePolicy() with node dependent on randNumber
		
		Random random = new Random();
		int randNumber = random.nextInt(10 - 1 + 1) + 1;
		
		OthelloState nodetmp;
		if(randNumber == 1) {
			nodetmp = bestChild(state.children.get(0));
			return treePolicy(nodetmp);
		} else {
			nodetmp = bestChild(state);
			return treePolicy(nodetmp);
		}
	}
	
	private OthelloState bestChild(OthelloState state) {
		int bestValuePlayer1 = Integer.MIN_VALUE;
		int bestValuePlayer2 = Integer.MAX_VALUE;
		
		OthelloState bestChild = null;
		
		// PLAYER1, return child with max score
		if(state.nextPlayerToMove == 0) {
			for (int i = 0; i < state.children.size(); i++) {
				if (state.children.get(i).avgScore > bestValuePlayer1) {
					bestValuePlayer1 = state.children.get(i).avgScore;
					bestChild = state.children.get(i);
				}
			}
			return bestChild;
		}
		// PLAYER2, return child with min score
		else if (state.nextPlayerToMove == 1) {
			for (int i = 0; i < state.children.size(); i++) {
				if (state.children.get(i).avgScore < bestValuePlayer2) {
					bestValuePlayer2 = state.children.get(i).avgScore;
					bestChild = state.children.get(i);
				}
			}
			return bestChild;
		}
		
		System.out.println("Couldn't find best child?");
		return null;
	}

	
}






