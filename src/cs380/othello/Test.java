package cs380.othello;

/**
 *
 * @author santi
 */
public class Test {

	public static void main(String args[]) {
		int iwon = 0;
		int pcwon = 0;
		int ties = 0;
		int numberOfGames = 1;
		int iterations = 10000;
		
		if (args.length == 0) {
			System.out.println("Error: Too few arguements. Run using either:\n $ java Test minimax <(optinal)#games>\n $ java Test mcts <#iterations> <(optional)#games>");
		}
		
		if (args[0] == "minimax") {
			if (args.length > 2) {
				System.out.println("Error: Too many arguements.\nRun using: $ java Test minimax <(optional)#games>");
				return;
			}
			if (args.length == 2) {
				numberOfGames = Integer.parseInt(args[1]);
			}
			for (int i = 0; i < numberOfGames; i++) {
				OthelloState state = new OthelloState(8);
				OthelloPlayer players[] = {new OthelloRandomPlayer(),
										   new OthelloPlayerBrett(4)};

				do{
					// Display the current state in the console:
					System.out.println("\nCurrent state, " + OthelloState.PLAYER_NAMES[state.nextPlayerToMove] + " to move:");
					System.out.print(state);

					// Get the move from the player:
					OthelloMove move = players[state.nextPlayerToMove].getMove(state);            
					System.out.println(move);
					state = state.applyMoveCloning(move);            
				} while(!state.gameOver());

				// Show the result of the game:
				System.out.println("\nFinal state with score: " + state.score());
				System.out.println(state);

				if (state.score() < 0) {
					iwon++;
				} else if (state.score() > 0) {
					pcwon++;
				} else {
					ties++;
				}
			} 
			System.out.println("______________________________");
			System.out.println("RESULTS after " + numberOfGames + " simulation(s):\n");
			System.out.println("                Wins | Percent");
			System.out.println("Minimax Player :  " + iwon + "  |   " + Math.round(100*(iwon)/(iwon + pcwon + ties)) + "%");
			System.out.println("Random  Player :  " + pcwon + "  |   " + Math.round(100*(pcwon)/(iwon + pcwon + ties)) + "%");
			System.out.println("Ties           :  " + ties + "  |   " + Math.round(100*(ties)/(iwon + pcwon + ties)) + "%");
			System.out.println("______________________________");
			
			
		} else if (args[0] == "mcts") {
			if (args.length > 3) {
				System.out.println("Error: Too many arguements.\nRun using: $ java Test mcts <#iterations> <(optional)#games>");
				return;
			}
			if (args.length < 2) {
				System.out.println("Error: Too few arguements.\nRun using: $ java Test mcts <#iterations> <(optional)#games>");
				return;
			}
			if (args.length == 3) {
				numberOfGames = Integer.parseInt(args[2]);
			}
			iterations = Integer.parseInt(args[1]);
			
			for (int i = 0; i < numberOfGames; i++) {
				OthelloState state = new OthelloState(8);
				OthelloPlayer players[] = {new OthelloRandomPlayer(),
										   new OthelloPlayerMCTS(iterations)};

				do{
					// Display the current state in the console:
					System.out.println("\nCurrent state, " + OthelloState.PLAYER_NAMES[state.nextPlayerToMove] + " to move:");
					System.out.print(state);

					// Get the move from the player:
					OthelloMove move = players[state.nextPlayerToMove].getMove(state);            
					System.out.println(move);
					state = state.applyMoveCloning(move);            
				} while(!state.gameOver());

				// Show the result of the game:
				System.out.println("\nFinal state with score: " + state.score());
				System.out.println(state);

				if (state.score() < 0) {
					iwon++;
				} else if (state.score() > 0) {
					pcwon++;
				} else {
					ties++;
				}
			} 
			System.out.println("______________________________");
			System.out.println("RESULTS after " + iterations + " simulation(s):\n");
			System.out.println("                Wins | Percent");
			System.out.println("MCTS Player    :  " + iwon + "  |   " + Math.round(100*(iwon)/(iwon + pcwon + ties)) + "%");
			System.out.println("Random  Player :  " + pcwon + "  |   " + Math.round(100*(pcwon)/(iwon + pcwon + ties)) + "%");
			System.out.println("Ties           :  " + ties + "  |   " + Math.round(100*(ties)/(iwon + pcwon + ties)) + "%");
			System.out.println("______________________________");
			
		} else {
			System.out.println("First arguement must be algorithm type: minimax, mcts");
			return;
		}
		return;

		
	}
}
